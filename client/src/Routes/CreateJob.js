import React, {Fragment} from 'react'
import {connect} from 'react-redux'

import {fetchUser, changeName,fetchJobs,createJob} from './../actions/index'

class CreateJob extends React.Component {

  constructor() {
    super()
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.state = {
      jobData: {
        id: '',
        name: ''
      }
    }
  }

  componentDidMount() {
    this.props.fetchUser()
    this.props.fetchJobs()
  }

  componentWillReceiveProps(props) {
    const { userData } = this.props.jobs;
    if ( userData ) {
      this.setState({
        jobData: {
          id: userData._id,
          name: ''
        }
      })
    }
  }

  handleChange(e) {
    e.preventDefault();
    this.setState({
      jobData: {
        ...this.state.jobData,
        [e.target.name]: e.target.value
      }
    })
  }

  handleSubmit(e) {
    e.preventDefault()
    this.props.createJob(this.state.jobData)
  }

  render() {
    const {userData, fetchingUser,jobsData,fetchingJobs} = this.props.jobs;
    return <Fragment>
      <h3>Role :        {userData ? userData.role : ''} </h3>
      <h3>Name :          {userData ? userData.name : ''}</h3>

      <form className="form-horizontal" onSubmit={this.handleSubmit}>
  <div className="form-group">
    <label className="control-label col-sm-2" for="name">Name:</label>
    <div className="col-sm-10">
      <input type="text" name="name" value={this.state.jobData.name} onChange={this.handleChange} className="form-control" id="name" placeholder="Enter name" />
      </div>
      </div>

  <div className="form-group"> 
    <div className="col-sm-offset-2 col-sm-10">
      <button type="submit" className="btn btn-default">create Job</button>
    </div>
  </div>
</form>
    

            <h1>Jobs</h1>
      {fetchingJobs ? 
      <div className="loader"></div> :
      <table className="table table-striped">
        <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Created At</th>
          <th scope="col">Name</th>
          <th scope="col">Created By</th>
          <th scope="col">Allocate To</th>
        </tr>
        </thead>
        <tbody>
          {jobsData.map((item, index) => <tr key={item.index}>
          <th scope="row">{index + 1}</th>
          <td>{item.created_at}</td>
          <td>{item.name}</td>
          <td>{item.createdBy.name}</td>
          <td>{item.allocateTo == null ? 'N/A': item.allocateTo.name}</td>

        </tr>)}
        
        </tbody>
      </table> }



</Fragment>;
  }
}

const mapStateToProps = (state) => ({jobs: {...state.jobs}});

const mapDispatchToProps = (dispatch) => ({
  fetchUser: () => dispatch(fetchUser()),
  fetchJobs: () => dispatch(fetchJobs()),
  createJob:(userData) => dispatch(createJob(userData)),
  changeName: (name) => dispatch(changeName(name))
});


export default connect(mapStateToProps, mapDispatchToProps)(CreateJob);