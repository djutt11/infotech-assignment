import React, {Fragment} from 'react'
import {connect} from 'react-redux'

import {fetchGroups} from './../actions/index'

class Home extends React.Component {
  componentDidMount() {
    this.props.fetchGroups()
  }
  render() {
    const {data, fetching} = this.props.jobs;
    return <Fragment>
      <h1>Groups</h1>
      {fetching ? 
      <div className="loader"></div> :
      <table className="table table-striped">
        <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Created At</th>
          <th scope="col">Name</th>
          <th scope="col">Notes</th>
          <th scope="col">User Count</th>
        </tr>
        </thead>
        <tbody>
          {data.map((item, index) => <tr key={item.index}>
          <th scope="row">{index + 1}</th>
          <td>{item.created_at}</td>
          <td>{item.name}</td>
          <td>{item.notes || 'N/A'}</td>
          <td>{item.users.length}</td>
        </tr>)}
        
        </tbody>
      </table> }
    </Fragment>;
  }
}

const mapStateToProps = (state) => ({jobs: {...state.jobs}});

const mapDispatchToProps = (dispatch) => ({
  fetchGroups: () => dispatch(fetchGroups())
});


export default connect(mapStateToProps, mapDispatchToProps)(Home);