import React, {Fragment} from 'react'
import {connect} from 'react-redux'

import {fetchManager,fetchJobs,fetchUsers,reallocate} from './../actions/index'

class ManagerJob extends React.Component {


  constructor(props) {
    
    super(props)
    this.handleChange = this.handleChange.bind(this)
    this.handleChange2 = this.handleChange2.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.state = {
      errorMsg: '',
      jobData: {
        userId: '',
        jobId: '',
      }
    }
  }


  handleChange(e,name) {
    e.preventDefault();
    this.setState({
      jobData: {
        ...this.state.jobData,
        [name] : e.target.value,
      }
    })
  }

  handleChange2(e) {
    e.preventDefault();
    this.setState({
      jobData: {
        ...this.state.jobData,
        userId : e.target.value
      }
    })
  }

  handleSubmit(e) {
    e.preventDefault()
    console.log(this.state);

    if(this.state.jobData.jobId == '' || this.state.jobData.userId == ''){
      this.setState({errorMsg:"select an options"})
    }else{
     this.props.reallocate(this.state.jobData);
    }
  }
  
  componentWillMount() {
    this.props.fetchManager();
    this.props.fetchJobs();
    this.props.fetchUsers();
  }

  componentWillReceiveProps(newProps) {

  }
  render() {
    const {managerData, fetchingManager,jobsData=[],fetchingJobs,usersData=[],relJobData} = this.props.jobs;
    return <Fragment>
      <h3>Role :        {managerData ? managerData.role : ''} </h3>
      <h3>Name :          {managerData ? managerData.name : ''}</h3>
      {this.state.errorMsg ? this.state.errorMsg : ''}
      {JSON.stringify(relJobData)}
    
      <form className="form-horizontal" onSubmit={this.handleSubmit}>

        <div className="form-group">
          <label>Select Task </label>
          <select id="exampleFormControlSelect1" className="form-control" value={this.state.jobData.jobId} onChange={(e)=>this.handleChange(e,"jobId")}>
            {jobsData.map(function(item) {
            return (
            <option key={item._id} value={item._id}>{item.name}</option>
            );
            })}
          </select>
        </div>


        <div className="form-group">
          <label>Select User</label>
          <select id="exampleFormControlSelect2" className="form-control" value={this.state.jobData.userId} onChange={(e)=>this.handleChange(e,"userId")}>
            {usersData.map(function(item,i) {
            return (
            <option key={item._id} value={item._id}>{item.name}</option>
            );
            })}
          </select>
        </div>


      <div className="form-group"> 
        <div className="col-sm-offset-2 col-sm-10">
          <button type="submit" className="btn btn-default">reallocate</button>
        </div>
      </div>
      </form>



            <h1>Jobs</h1>
      {fetchingJobs ? 
      <div className="loader"></div> :
      <table className="table table-striped">
        <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Created At</th>
          <th scope="col">Name</th>
          <th scope="col">Created By</th>
          <th scope="col">Allocate To</th>
        </tr>
        </thead>
        <tbody>
          {jobsData.map((item, index) => <tr key={item.index}>
          <th scope="row">{index + 1}</th>
          <td>{item.created_at}</td>
          <td>{item.name}</td>
          <td>{item.createdBy.name}</td>
          <td>{item.allocateTo == null ? 'N/A': item.allocateTo.name}</td>

        </tr>)}
        
        </tbody>
      </table> }





    </Fragment>;
  }
}

const mapStateToProps = (state) => ({jobs: {...state.jobs}});

const mapDispatchToProps = (dispatch) => ({
  fetchManager: () => dispatch(fetchManager()),
  fetchJobs: () => dispatch(fetchJobs()),
  fetchUsers:() => dispatch(fetchUsers()),
  reallocate:(userData)=>dispatch(reallocate(userData)),
});


export default connect(mapStateToProps, mapDispatchToProps)(ManagerJob);