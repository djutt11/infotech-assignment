const initialState = {
  data: [],
  jobsData:[],
  fetching: false,
  fetchingJobs: false,
  creatingJobs: false,
  fetchingUsers:false,
  reallocateJobs:false,
  name: '',
};

const jobs = (state = initialState, action) => {
  switch (action.type) {
    case 'NAME':
      return {
        ...state,
        name: action.name,
      }
      case 'FETCHING_GROUPS':
      return {
        ...state,
        fetching: true,
      }
      case 'GROUPS':
      return {
        ...state,
        data: action.data,
        fetching: false,
      }
      case 'ERROR':
      return {
        ...state,
        error: action.data,
        fetching: false,
      }

      case 'FETCHING_JOBS':
      return {
        ...state,
        fetchingJobs: true,
      }
      case 'JOBS':
      return {
        ...state,
        jobsData: action.data,
        fetchingJobs: false,
      }
      case 'JOBS_ERROR':
      return {
        ...state,
        jobsError: action.data,
        fetchingJobs: false,
      }

      case 'CREATE_JOB':
      return {
        ...state,
        creatingJobs: true,
      }
      case 'NEW_JOB':
      return {
        ...state,
        newJobData: action.data,
        creatingJobs: false,
      }

      case 'REALLOCATE_JOB':
      return {
        ...state,
        reallocateJobs: true,
      }
      case 'REL_JOB':
      return {
        ...state,
        relJobData: action.data,
        reallocateJobs: false,
      }

      

      case 'FETCHING_USER':
      return {
        ...state,
        fetchingUser: true,
      }
      case 'USER':
      return {
        ...state,
        userData: action.data,
        fetchingUser: false,
      }

      case 'FETCHING_USERS':
      return {
        ...state,
        fetchingUsers: true,
      }
      case 'USERS':
      return {
        ...state,
        usersData: action.data,
        fetchingUsers: false,
      }

      case 'USER_ERROR':
      return {
        ...state,
        userError: action.data,
        fetchingUser: false,
        fetchingUsers: false,
      }
      case 'FETCHING_MANAGER':
      return {
        ...state,
        fetchingManager: true,
      }
      case 'MANAGER':
      return {
        ...state,
        managerData: action.data,
        fetchingManager: false,
      }
      case 'MANAGER_ERROR':
      return {
        ...state,
        managerError: action.data,
        fetchingManager: false,
      }
    default:
      return state
  }
}

export default jobs