import axios from 'axios';
export const changeName = name => ({
  type: 'NAME',
  name
})


export const createJob = (userData) => {
  return dispatch => {
    dispatch({type: 'CREATE_JOB'});
    axios.post('http://localhost:9000/jobs',
    {
      name: userData.name,
      createdBy: userData.id,
    }
  )
  .then(function (response) {
        dispatch({type: 'NEW_JOB', data: response.data.data});
        dispatch(fetchJobs())
      })
      .catch(function (error) {
        dispatch({type: 'JOBS_ERROR', data: error});
      });
  };
}


export const reallocate = (userData) => {
  return dispatch => {
    dispatch({type: 'REALLOCATE_JOB'});
    axios.post('http://localhost:9000/managers/reallocate',
    {
      userId: userData.userId,
      jobId: userData.jobId,
    }
  )
  .then(function (response) {
        dispatch({type: 'REL_JOB', data: response.data.data});
        dispatch(fetchJobs())
      })
      .catch(function (error) {
        dispatch({type: 'JOBS_ERROR', data: error});
      });
  };
}



export const fetchJobs = () => {
  return dispatch => {
    dispatch({type: 'FETCHING_JOBS'});
    axios.get('http://localhost:9000/jobs')
      .then(function (response) {
        dispatch({type: 'JOBS', data: response.data.data});

      })
      .catch(function (error) {
        dispatch({type: 'JOBS_ERROR', data: error});
      });
  };
}

export const fetchGroups = () => {
  return dispatch => {
    dispatch({type: 'FETCHING_GROUPS'});
    axios.get('http://localhost:9000/groups')
      .then(function (response) {
        dispatch({type: 'GROUPS', data: response.data.data});

      })
      .catch(function (error) {
        dispatch({type: 'ERROR', data: error});
      });
  };
}

export const fetchUser = () => {
  return dispatch => {
    dispatch({type: 'FETCHING_USER'});
    axios.get('http://localhost:9000/users/User')
      .then(function (response) {
        dispatch({type: 'USER', data: response.data.data});

      })
      .catch(function (error) {
        dispatch({type: 'USER_ERROR', data: error});
      });
  };
}

export const fetchUsers = () => {
  return dispatch => {
    dispatch({type: 'FETCHING_USERS'});
    axios.get('http://localhost:9000/users')
      .then(function (response) {
        dispatch({type: 'USERS', data: response.data.data});

      })
      .catch(function (error) {
        dispatch({type: 'USER_ERROR', data: error});
      });
  };
}

export const fetchManager = () => {
  return dispatch => {
    dispatch({type: 'FETCHING_MANAGER'});
    axios.get('http://localhost:9000/managers/Manager')
      .then(function (response) {
        dispatch({type: 'MANAGER', data: response.data.data});

      })
      .catch(function (error) {
        dispatch({type: 'MANAGER_ERROR', data: error});
      });
  };
}

export const VisibilityFilters = {
  FETCHING_GROUPS: 'FETCHING_GROUPS',
  GROUPS: 'GROUPS',
  ERROR: 'ERROR'
}