var express = require("express");

appRoute = new express.Router();

const groupController = require('./controllers/default.controller');

appRoute.use('', groupController);
module.exports = appRoute;