var express = require("express");
var ioc = require('../../../ioc');
var groupService = require('../services/group.service');
appRoutes = new express.Router();

appRoutes.get("", function(req, res){
    groupService.list().then(function(records){
        return res.json(ioc.res.success("Groups", records));
    }).catch(function(error){
        return res.json(ioc.res.error("Please fix following errors", error));
    });
});

appRoutes.post("", function(req, res){
    var params = {name:req.body.name};
    groupService.persist(params).then(function(record){
        return res.json(ioc.res.success("Groups", record));
    }).catch(function(error){
        return res.json(ioc.res.error("Please fix following errors", error));
    });
});


module.exports = appRoutes;