
'use strict';

const GroupModel = require('../models/group.schema');
const ioc = require('../../../ioc');


const persist= function (group) {
    var groupModel = new GroupModel(group);
    return groupModel.save();
};

const findById= function (id) {
    return GroupModel.findById(id).exec();
};


const findByName = function(name){
    return GroupModel.find({"name":name}).exec();
}


var list = function () {
    return GroupModel.find().populate("users").exec();
};

var addUser = function(groupId,userId){
    return findById(groupId).then(function (group) {
        var users=group.users || [];
        users.push(userId);
        group.users=users;
        group.save();
    });
}

module.exports = {
    'persist':persist,
    'findById':findById,
    'findByName':findByName,
    'list':list,
    'addUser':addUser
};
