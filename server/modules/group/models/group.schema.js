'use strict';

var mongoose = require('mongoose');
var Schema =mongoose.Schema;



var groupSchema = new Schema(
    {

        name: String,
        notes:String,
        "users": [{
            type: Schema.Types.ObjectId,
            ref: 'Users'
        }]
    },
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

var Groups = mongoose.model('Groups',groupSchema);

module.exports = Groups;
