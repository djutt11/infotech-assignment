var express = require("express");

appRoute = new express.Router();

const jobRoutes = require('./job/routes');
const groupRoutes = require('./group/routes');
const userRoutes = require('./user/routes');

appRoute.use('/jobs', jobRoutes);
appRoute.use('/groups', groupRoutes);
appRoute.use('/users', userRoutes);

module.exports = appRoute;