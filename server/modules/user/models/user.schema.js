'use strict';

var mongoose = require('mongoose');
var Schema =mongoose.Schema;

var usersSchema = new Schema(
    {
        name: String,
        role: String,
        "jobs": [{
            type: Schema.Types.ObjectId,
            ref: 'Jobs'
        }],
        count: { type: Number, default: 0 }
    },
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

var Users = mongoose.model('Users',usersSchema);

module.exports = Users;
