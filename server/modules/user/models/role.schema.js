'use strict';

var mongoose = require('mongoose');
var Schema =mongoose.Schema;

var roleSchema = new Schema(
    {
        type: String
    },
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

var Roles = mongoose.model('Roles',roleSchema);

module.exports = Roles;
