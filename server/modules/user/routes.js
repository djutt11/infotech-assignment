var express = require("express");

appRoute = new express.Router();

const defaultController = require('./controllers/default.controller');
const managerController = require('./controllers/manager.controller');

appRoute.use('/users', defaultController);
appRoute.use('/managers', managerController);
module.exports = appRoute;