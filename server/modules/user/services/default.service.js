'use strict';

const UserModel = require('../models/user.schema');
const ioc = require('../../../ioc');

const persist= function (user) {

    var userModel = new UserModel(user);
    return userModel.save();
};

const findById= function (id) {
    return UserModel.findById(id).exec();
}
const findByName= function (name) {
    return UserModel.findOne({'name':name}).exec();
}


var list = function () {
    return UserModel.find().populate('groups').exec();
};

var addGroup= function (userId,groupId) {
    return findById.then(function (user) {
        var groups=user.groups || [];
        groups.push(groupId);
        user.groups=groups;
         user.save();
    });
}

module.exports = {
    'persist':persist,
    'findById':findById,
    'list':list,
    'addGroup':addGroup,
    'findByName':findByName
};
