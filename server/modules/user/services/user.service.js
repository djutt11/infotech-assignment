'use strict';

const UserModel = require('../models/user.schema');
const ioc = require('../../../ioc');
const defaultService= require('./default.service');

const persist= function (user) {
    var params = {"name": user.name, 'role' : 'User', groups : []};
    ioc.logger.info("Saving suer Details", user);
    return defaultService.persist(params)

};

const findById= function (id) {
    return UserModel.findById(id).exec();
}

const findByName= function (name) {
    return UserModel.findOne({'name':name,'role':'User'}).exec();
}


var list = function () {
    return UserModel.find({'role':'User'}).populate("jobs").exec();
};

var addGroup= function (userId,groupId) {
    return findById(userId).then(function (user) {
        var groups=user.groups;
        groups.push(groupId);
        user.groups=groups;
        user.save();
    });
}

var getAssignTaskUser = function(){
    return UserModel.find({'role':'User'}).sort({count: 1})
}

var allocateTask = function (jobId,userId) {
    return findById(userId).then(function (user) {
        var jobs =user.jobs || [];
        jobs.push(jobId);
        user.jobs=jobs;
        user.count =jobs.length;
        user.save();
    });
}


module.exports = {
    'persist':persist,
    'findById':findById,
    'list':list,
    'addGroup':addGroup,
    'findByName':findByName,
    'getAssignTaskUser':getAssignTaskUser,
    'allocateTask':allocateTask
};
