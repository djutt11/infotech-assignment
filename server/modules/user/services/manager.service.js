'use strict';

const UserModel = require('../models/user.schema');
const defaultService= require('./default.service');
const ioc = require('../../../ioc');

const persist= function (user) {
    var params = {"name": user.name, 'role' : 'Manager', groups : []};
    ioc.logger.info("Saving suer Details", user);
    return defaultService.persist(params)

};

const findById= function (id) {
    return UserModel.findById(id).exec();
}
const findByName= function (name) {
    return UserModel.findOne({'name':name,'role':'Manager'}).exec();
}


var list = function () {
    return UserModel.find({'role':'Manager'}).populate("groups").exec();};

var addGroup= function (userId,groupId) {
    return findById.then(function (user) {
        var groups=user.groups;
        groups.push(groupId);
        user.groups=groups;
        user.save();
    });
}

module.exports = {
    'persist':persist,
    'findById':findById,
    'list':list,
    'addGroup':addGroup,
    'findByName':findByName
};
