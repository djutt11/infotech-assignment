var express = require("express");
var ioc = require('../../../ioc');
var userService = require('../services/user.service');

appRoutes = new express.Router();

appRoutes.get("", function(req, res){
    userService.list().then(function(user){
        return res.json(ioc.res.success("User Created Successfully", user));

    }).catch(function(error){
        return res.json(ioc.res.error("Please fix following errors", error));
    });
});

appRoutes.get("/User", function(req, res){
    userService.findByName("Asad").then(function(user){
        return res.json(ioc.res.success("login by User", user));

    }).catch(function(error){
        return res.json(ioc.res.error("Please fix following errors", error));
    });
});

appRoutes.post("", function(req, res){
    var params = {name : req.body.name};
    userService.persist(params).then(function(user){
        return res.json(ioc.res.success("User Created Successfully", user));
    }).catch(function(error){
        return res.json(ioc.res.error("Please fix following errors", error));
    });
});


module.exports = appRoutes;