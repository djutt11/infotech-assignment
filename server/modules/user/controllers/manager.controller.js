var express = require("express");
var ioc = require('../../../ioc');
var managerService = require('../services/manager.service');
var userService = require('../services/user.service');

var jobService = require('../../job/services/default.service');

appRoutes = new express.Router();

appRoutes.get("", function(req, res){
    managerService.list().then(function(user){
        return res.json(ioc.res.success("Manager List", user));

    }).catch(function(error){
        return res.json(ioc.res.error("Please fix following errors", error));
    });
});
appRoutes.get("/Manager", function(req, res){
    managerService.findByName("ISK").then(function(user){
        return res.json(ioc.res.success("login by User", user));

    }).catch(function(error){
        return res.json(ioc.res.error("Please fix following errors", error));
    });
});
appRoutes.post("", function(req, res){
    var params = {name : req.body.name};
    managerService.persist(params).then(function(user){
        return res.json(ioc.res.success("Manager Created Successfully", user));
    }).catch(function(error){
        return res.json(ioc.res.error("Please fix following errors", error));
    });
});

appRoutes.post("/reallocate", function(req, res){
    var jobId=req.body.jobId;
    var userId=req.body.userId
    console.log('jobId',jobId);
    console.log('userId',userId);

    jobService.findByAllocate(jobId).then(function(job){
        console.log('jobs  ',job);
        if(job){
            jobService.findById(jobId).then(function(result){
                result.allocateTo=userId
                result.save();
                    userService.allocateTask(result.id,userId).then(function(user){
                        return res.json(ioc.res.success("Job assigned Successfully", result));        
                    });
            })
        }else{
            var result=[{'name':" conflict occure cannot assign task"}];
            return res.json(ioc.res.success("cannot assign task ",result));
        }
    }).catch(function(error){
        return res.json(ioc.res.error("Please fix following errors", error));
    });
});




module.exports = appRoutes;