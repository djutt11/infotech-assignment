'use strict';

var mongoose = require('mongoose');
var Schema =mongoose.Schema;

var jobsSchema = new Schema(
    {
         name: String,
         notes: String,
        "createdBy": {
            type: Schema.Types.ObjectId,
            ref: 'Users'
        },
        "allocateTo": {
            type: Schema.Types.ObjectId,
            ref: 'Users'
        }
    },
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

var Jobs = mongoose.model('Jobs',jobsSchema);

module.exports = Jobs;
