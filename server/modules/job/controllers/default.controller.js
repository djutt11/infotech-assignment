var express = require("express");
var ioc = require('../../../ioc');
var jobService = require('../services/default.service');
var userService = require('../../user/services/user.service');

appRoutes = new express.Router();

appRoutes.get("", function(req, res){
    jobService.list().then(function(results){
        return res.json(ioc.res.success("Job created Successfully", results));
    }).catch(function(error){
        return res.json(ioc.res.error("Please fix following errors", error));
    });
});

appRoutes.post("", function(req, res){
    var createdBy = req.body.createdBy;

    var job = {'name' : req.body.name, "createdBy" : createdBy};
    jobService.persist(job).then(function(result){
        jobService.findById(result.id).then(function(job){
            userService.getAssignTaskUser().then(function(users){
                job.allocateTo=users[0].id
                job.save();
                userService.allocateTask(result.id,users[0].id).then(function(result){
                    return res.json(ioc.res.success("Job created Successfully", job));        
                })
            });
        })
    }).catch(function(error){
        return res.json(ioc.res.error("Please fix following errors", error));
    });
});

appRoutes.post("/:jobId/allocate", function(req, res){
    var jobId = req.params.jobId;
    var allocateTo = req.body.params;
    jobService.allocate(jobId, allocateTo).then(function(result){
        jobService.findById(result.id).then(function(j){
            return res.json(ioc.res.success("Job created Successfully", j));
        })
    }).catch(function(error){
        return res.json(ioc.res.error("Please fix following errors", error));
    });
});

module.exports = appRoutes;