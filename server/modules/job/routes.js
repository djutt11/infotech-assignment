var express = require("express");

appRoute = new express.Router();

const defaultController = require('./controllers/default.controller');

appRoute.use('', defaultController);
module.exports = appRoute;