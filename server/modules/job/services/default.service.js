'use strict';

const JobModel = require('../models/job.schema');
const ioc = require('../../../ioc');

const persist= function (job) {
    var params = {"name": job.name, "notes" : job.notes, "createdBy" : job.createdBy,"allocateTo":null};
    var jobModel = new JobModel(params);
    return jobModel.save();
};

const findById= function (id) {
    return JobModel.findById(id).populate("createdBy").exec();
}
const findByName= function (name) {
    return JobModel.findOne({'name':name}).exec();
}

const findByAllocate = function(jobId){
    return JobModel.findOne({'_id':jobId,'allocateTo':null}).exec();
}

const allocate = function (jobId,userId) {
    return findById(jobId).then(function (job) {
        job.allocateTo = userId ;
        job.save();
    })
}



var list = function () {
    return JobModel.find().populate("createdBy").populate("allocateTo").exec();
};

module.exports = {
    'persist':persist,
    'findById':findById,
    'list':list,
    'allocate':allocate,
    'findByName':findByName,
    'findByAllocate':findByAllocate
};
