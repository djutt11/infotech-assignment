require('dotenv').config();

const mongoose = require("mongoose");
const async = require("async");

let connectionString = process.env.monogo_uri;

console.log("MONGODB URL", connectionString);
mongoose.connect(connectionString);
mongoose.promise= global.promise;

const groupService = require('./modules/group/services/group.service');
const userService = require('./modules/user/services/user.service');
const managerService = require('./modules/user/services/manager.service');
const jobService = require('./modules/job/services/default.service');

let db = mongoose.connection;
db.on('error',function(error){
    console.log('db connection error ',error)
});

db.once('open',function(){
    console.log("DATABASE CONNECTED Successfully.. Now Inserting Test Data");
    groups = ["NodeJS", "HR", "Admin"];
    users = ["Danial", "Fahad", 'Asad', 'Fazeel'];
    managers = ["ISK"];
    jobs =["Job1","Job2","Job3","job4"]


/*
    var createGroups = function(){
        groups.forEach(group => {
            groupService.findByName(group).then(function(result){
                if(result){
                    console.log("Group found",group);
                    users.forEach(user => {
                        userService.findByName(user).then(function(found){
                            if(found){
                                groupService.addUser(result.id,found.id).then(function(user){
                                    console.log('user add into group',user);
                                });
                                console.log("User Found ", user);
                            }else{

                                console.log(" User Not found", user)
                            }
                        });
                    });
                }else{
                    console.log("No group found ",group);                  
                }
            }).catch(function(error){
                console.log(error)
            });           
        });
    };

    createGroups();
*/



var createJobs = function(userId){
    jobs.forEach(job => {
        jobService.findByName(job).then(function(found){
            if(found){
                console.log("Job Found ", job);
            }else{
                console.log("Creating Job", job)
                var record = {'name' : job,'createdBy':userId};
                jobService.persist(record).then(function(g){
                    console.log("Job :: " + g.name);
                });
            }
        });
    });
}


userService.findByName("Asad").then(function(user){
    createJobs(user.id);

});

});

