const winston = require('winston');

/**
 * Logging
 * **/

let logFile = process.env.logger_file || 'app.log';
let wLogger = new (winston.Logger)({
    level: process.env.logger_level,
    transports: [
        new (winston.transports.Console)(),
        new(winston.transports.File)({filename: logFile})
    ]
});

let container = {};
container.logger = wLogger;
container.log = wLogger;
container.res = {};
container.res.success = function(message, data){
    return {code:200, 'message' :  message, 'data' : data};
}
container.res.error = function(message, errors){
    return {code:400, 'message' :  message, 'errors' : errors};
}

module.exports = container;