require('dotenv').config();

const mongoose = require("mongoose");
const async = require("async");

let connectionString = process.env.monogo_uri;

console.log("MONGODB URL", connectionString);
mongoose.connect(connectionString);
mongoose.promise= global.promise;

const groupService = require('./modules/group/services/group.service');
const userService = require('./modules/user/services/user.service');
const managerService = require('./modules/user/services/manager.service');
const jobService = require('./modules/job/services/default.service');

let db = mongoose.connection;
db.on('error',function(error){
    console.log('db connection error ',error)
});

db.once('open',function(){
    console.log("DATABASE CONNECTED Successfully.. Now Inserting Test Data");
    groups = ['Support', "Team Lead", "NodeJS", "HR", "Admin"];
    users = ["Danial", "Fahad", 'Asad', 'Fazeel'];
    managers = ["ISK"];

    var createManagers = function(callback){
        managers.forEach(manager => {
            managerService.findByName(manager).then(function(result){
                if(result){
                    console.log("Manager found", manager);
                }else{
                    console.log("Creating New Manager :: ",manager);
                    var record = {'name' : manager};
                    managerService.persist(record).then(function(g){
                    });
                }
            });
        });
        callback(null, 'createGroups');
    };

    var createGroups = function(callback){
        groups.forEach(group => {
            groupService.findByName(group).then(function(result){
                if(result){
                    console.log("Group found",group);
                }else{
                    console.log("Creating Group ",group);
                    var record = {'name' : group, 'notes' : ''};
                    groupService.persist(record).then(function(g){
                        console.log("Created Group :: " + g.name);
                    });
                }
            });
        });
        callback(null, 'createGroups');

    };

    var createUsers = function(callback){
        users.forEach(user => {
            userService.findByName(user).then(function(found){
                if(found){
                    console.log("User Found ", user);
                }else{
                    console.log("Creating User", user)
                    var record = {'name' : user};
                    userService.persist(record).then(function(g){
                        console.log("user :: " + g.name);
                    });
                }
            });
        });
        callback(null, 'createUsers');
    }

    async.series([createManagers, createGroups, createUsers], function(error, result){
        console.log("Groups created, User Created...");
    });



});

