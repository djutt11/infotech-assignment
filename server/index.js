require('dotenv').config();
const express= require('express');
const mongoose= require('mongoose');
const cors =require('cors');

const bodyParser = require('body-parser');
const appRoutes = require("./modules/routes");
const ioc = require('./ioc');

const app =new  express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.use(cors());

app.use('/v1',appRoutes);
app.use('/',appRoutes);

let connectionString = process.env.monogo_uri;
mongoose.connect(connectionString);
mongoose.promise= global.promise;

let db=mongoose.connection;
db.on('error',function(error){
    console.log('db connection error ',error)
});

db.once('open',function(){
    console.log('db is connected ',connectionString)

    app.listen(process.env.server_port,process.env.server_host,function () {
        console.log("You app is running on :: http://"+ process.env.server_host + ":" + process.env.server_port + "/");
    });


});

